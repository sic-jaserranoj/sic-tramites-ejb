/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sic.gov.co.tramites.SicTramitePersona;


/**
 *
 * @author ingeniun
 */
@Stateless
@Path("sictramitepersona")
public class SicTramitePersonaFacadeREST extends AbstractFacade<SicTramitePersona> {

    @PersistenceContext(unitName = "tramitesPU")
    private EntityManager em;

    public SicTramitePersonaFacadeREST() {
        super(SicTramitePersona.class);
    }

    @POST
    @Override
    @Consumes({"application/json;charset=utf-8"})
    public void create(SicTramitePersona entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json;charset=utf-8"})
    public void edit(@PathParam("id") BigDecimal id, SicTramitePersona entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") BigDecimal id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json;charset=utf-8"})
    public SicTramitePersona find(@PathParam("id") BigDecimal id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json;charset=utf-8"})
    public List<SicTramitePersona> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json;charset=utf-8"})
    public List<SicTramitePersona> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces({"application/json;charset=utf-8"})
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
