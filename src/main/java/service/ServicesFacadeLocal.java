/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;


import java.util.Date;
import java.util.List;
import javax.ejb.Local;

import sic.gov.co.tramites.SicEmpreados;


/**
 * <b>Descripción:</b> 
 * <b>Proyecto:</b> 
 * 
 *
 * @author Ing.Jorge Serrano 
 * @version 1.0
 */
@Local
public interface ServicesFacadeLocal {
    
     /**
     * Obtiene los web service requeridos.
     * 
     *
     * @param service
     * @return Obtiene el listado de Vencidos por Días.
     */
   
    
    
      public List<SicEmpreados> consultarEmpleado(String usuario, String contrasena) ;
  
}
