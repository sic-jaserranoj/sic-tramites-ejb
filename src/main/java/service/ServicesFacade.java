
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.CacheRetrieveMode;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.eclipse.persistence.config.QueryHints;
import sic.gov.co.tramites.SicEmpreados;

/**
 * <b>Descripción:</b> Clase que implementa los servicios disponibles de la
 * 
 * <b>Proyecto:</b>
 *
 * @author Ing.Jorge Serrano
 * @version 1.0
 */
@Stateless
public class ServicesFacade implements ServicesFacadeLocal {

    @PersistenceContext(unitName = "tramitesPU")
    private EntityManager em;

    @Override
    public List<SicEmpreados> consultarEmpleado(String usuario, String contrasena) {
        List<SicEmpreados> listado;
        TypedQuery<SicEmpreados> parametroQuery = em.createNamedQuery("SicEmpreados.loginUsuario", SicEmpreados.class);
        parametroQuery.setParameter("idUsuario", usuario);
        parametroQuery.setParameter("contrasena", contrasena);

        try {

            listado = parametroQuery.getResultList();
            //listado = territorioQuery.setHint(QueryHints.CACHE_RETRIEVE_MODE, CacheRetrieveMode.BYPASS).getResultList();
        } catch (Exception ex) {

            listado = new ArrayList<>();
        }

        return listado;
    }


    
}
