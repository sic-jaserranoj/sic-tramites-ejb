/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import sic.gov.co.tramites.SicEmpreados;

/**
 *
 * @author ingeniun
 */
@Stateless
@Path("sicempreados")
public class SicEmpreadosFacadeREST extends AbstractFacade<SicEmpreados> {

    @PersistenceContext(unitName = "tramitesPU")
    private EntityManager em;
    
    @EJB
    private ServicesFacadeLocal busquedaServicio;

    public SicEmpreadosFacadeREST() {
        super(SicEmpreados.class);
    }
    
   
    @POST
    @Override
    @Consumes({"application/json;charset=utf-8"})
    public void create(SicEmpreados entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json;charset=utf-8"})
    public void edit(@PathParam("id") BigDecimal id, SicEmpreados entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") BigDecimal id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json;charset=utf-8"})
    public SicEmpreados find(@PathParam("id") BigDecimal id) {
        return super.find(id);
    }
    
   
    @GET
    @Override
    @Produces({"application/json;charset=utf-8"})
    public List<SicEmpreados> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("/consultar/{id}/{td}")
    @Produces({"application/json;charset=utf-8"})
    public List<SicEmpreados> consultarDocumento(@PathParam("id") String usuario, @PathParam("td") String contrasena) {
        return busquedaServicio.consultarEmpleado(usuario,contrasena);
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json;charset=utf-8"})
    public List<SicEmpreados> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces({"application/json;charset=utf-8"})
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
